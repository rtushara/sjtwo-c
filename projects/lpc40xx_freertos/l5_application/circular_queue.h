#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* In this part, the queue memory is statically defined
 * and fixed at compile time for 100 uint8s
 */
typedef struct {
  uint8_t *queue_memory;
  uint8_t pop;
  uint8_t push;
  uint8_t max_size;
  uint8_t current_size;
} queue_s;

// This should initialize all members of queue_s
bool queue__init(queue_s *queue, uint8_t *queue_memory_Ptr, uint16_t size);
// @returns false if the queue is full
bool queue__full(queue_s *queue);

// @returns false if the queue was empty
bool queue__empty(queue_s *queue);

bool queue__push(queue_s *queue, uint8_t push_value);

bool queue__pop(queue_s *queue, uint8_t *pop_value);

uint16_t queue__get_item_count(const queue_s *queue);