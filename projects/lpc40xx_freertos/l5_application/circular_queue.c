
#include "circular_queue.h"

bool queue__init(queue_s *queue, uint8_t *queue_memory_Ptr, uint16_t size) {
  if ((queue == NULL) || (size == 0u)) {
    return false;
  }
  queue->queue_memory = queue_memory_Ptr;
  queue->max_size = size;
  queue->current_size = 0u;
  queue->pop = 0u;
  queue->push = 0u;
  return true;
}
bool queue__push(queue_s *queue, uint8_t push_value) {
  if ((queue == NULL) || (queue->current_size == queue->max_size)) {
    return false;
  }
  queue->queue_memory[queue->push] = push_value;
  queue->push = (queue->push + 1u) % queue->max_size;
  queue->current_size++;
  return true;
}
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  if ((queue == NULL) || (queue->current_size == 0u)) {
    return false;
  }
  *pop_value = queue->queue_memory[queue->pop];
  queue->pop = (queue->pop + 1u) % queue->max_size;
  queue->current_size--;
  return true;
}
uint16_t queue__get_item_count(const queue_s *queue) {
  if (queue == NULL)
    return 0u;
  return queue->current_size;
}