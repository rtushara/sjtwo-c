#include "unity.h"

#include "Mockmessage.h"

#include "message_processor.h"

static bool message__read_stub(message_s *message_to_read, int call_count) {
  bool message_was_read = false;

  if (call_count >= 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = 't';
  }
  if (call_count == 1) {
    message_to_read->data[1] = 'r';
  }
  if (call_count == 2) {
    message_to_read->data[1] = 'm';
  }

  return message_was_read;
}

static bool message__readmsg_stub(message_s *message_to_read, int call_count) {
  bool message_was_read = false;

  if (call_count >= 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = 'p';
  }
  if (call_count == 1) {
    message_to_read->data[1] = 't';
  }
  if (call_count == 2) {
    message_to_read->data[1] = '$';
  }

  return message_was_read;
}

static bool message__readmsg1_stub(message_s *message_to_read, int call_count) {
  bool message_was_read = false;

  if (call_count >= 2) {
    message_was_read = false;
  } else {
    message_was_read = true;
  }

  if (call_count == 0) {
    message_to_read->data[0] = 'r';
  }
  if (call_count == 1) {
    message_to_read->data[1] = 's';
  }
  if (call_count == 2) {
    message_to_read->data[1] = '#';
  }

  return message_was_read;
}

// This only tests if we process at most 3 messages
void test_process_3_messages(void) {
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, false);
  message__read_IgnoreArg_message_to_read();

  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_FALSE(message_processor());
}

// This only tests if we process at most 3 messages
void test_process_messages_with_stubWithCallback(void) {
  // message_processor() makes a call to:
  // bool message__read(message_s *message_to_read);

  // Whenever message__read() occurs, it will go to your custom "stub" function
  // Once we stub, then each function call to message__read() will go to message__read_stub()
  message__read_StubWithCallback(message__read_stub);

  // Function under test
  message_processor();
}

void test_process_message_with_dollar_sign(void) {
  message__read_StubWithCallback(message__readmsg1_stub);

  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_without_any_dollar_sign(void) {
  message__read_StubWithCallback(message__readmsg1_stub);

  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_with_pound_sign(void) {
  message__read_StubWithCallback(message__readmsg1_stub);

  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_without_any_pound_sign(void) {
  message__read_StubWithCallback(message__readmsg_stub);

  TEST_ASSERT_FALSE(message_processor());
}