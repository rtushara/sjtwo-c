#include <stdio.h>
#include <string.h>

#include "unity.h"

/**
 * Generate "Mocks" for these files
 */

/**
 * Include the source we wish to test
 */
#include "circular_queue.h"

static queue_s my_queue;
static uint8_t my_queue_memory[200];

void setUp(void) {
  TEST_ASSERT_FALSE(queue__init(NULL, my_queue_memory, 200u));
  // queue__init(&my_queue, my_queue_memory, 200u);
  TEST_ASSERT_TRUE(queue__init(&my_queue, my_queue_memory, 200u));
}

void tearDown(void) {}

void test_comprehensive(void) {
  const uint16_t max_queue_size = 200u; // Change if needed

  // Test Queue push until it's full
  for (uint16_t item = 0u; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&my_queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&my_queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue__push(&my_queue, 123u));
  TEST_ASSERT_EQUAL(max_queue_size, queue__get_item_count(&my_queue));

  // Pull and verify the FIFO order
  for (uint16_t item = 0u; item < max_queue_size; item++) {
    uint8_t popped_value = 0u;
    TEST_ASSERT_TRUE(queue__pop(&my_queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Test wrap-around case
  const uint8_t pushed_value = 123u;
  TEST_ASSERT_TRUE(queue__push(&my_queue, pushed_value));
  uint8_t popped_value = 0u;
  TEST_ASSERT_TRUE(queue__pop(&my_queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count(&my_queue));
  TEST_ASSERT_FALSE(queue__pop(&my_queue, &popped_value));
}
