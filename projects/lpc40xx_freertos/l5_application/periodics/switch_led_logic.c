#pragma once

#include "stdio.h"

#include "FreeRTOS.h"
#include "board_io.h"
#include "gpio.h"
#include "task.h"

#include "switch_led_logic.h"

static gpio_s led0, led1, led2, led3, sw0, sw1, sw2, sw3;

void switch_led_logic__initialize(void) {
  led0 = board_io__get_led0();
  led1 = board_io__get_led1();
  led2 = board_io__get_led2();
  led3 = board_io__get_led3();
  sw0 = board_io__get_sw0();
  sw1 = board_io__get_sw1();
  sw2 = board_io__get_sw2();
  sw3 = board_io__get_sw3();
}

void switch_led_logic__run_once(void) {
  if (gpio__get(sw1)) {
    gpio__toggle(led1);
  } else {
    gpio__set(led1);
  }
}