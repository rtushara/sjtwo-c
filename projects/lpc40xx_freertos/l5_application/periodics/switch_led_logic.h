#pragma once

#include "stdio.h"

#include "FreeRTOS.h"
#include "board_io.h"
#include "gpio.h"
#include "task.h"

void switch_led_logic__initialize(void);

void switch_led_logic__run_once(void);