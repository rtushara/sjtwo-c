#include "steering_processor.h"

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {
  if ((left_sensor_cm && right_sensor_cm) >= threshold) {
    steer_processor__both_sensors_more_than_threshold();
  } else if (left_sensor_cm < right_sensor_cm) {
    steer_processor__move_right();
  } else if (left_sensor_cm > right_sensor_cm) {
    steer_processor__move_left();
  } else if ((left_sensor_cm && right_sensor_cm) < threshold) {
    steer_processor__both_sensors_less_than_threshold();
  }
}

void steer_processor__move_left(void) { steer_left(); }

void steer_processor__move_right(void) { steer_right(); }

void steer_processor__both_sensors_less_than_threshold(void) {}

void steer_processor__both_sensors_more_than_threshold(void) {}